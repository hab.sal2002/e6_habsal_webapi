﻿using System;
namespace TodoApi.Models;

public class TodoItems
{

    private int Id;
    private string? Name;
    private bool IsComplete;

    public TodoItems(int Id, string Name, bool IsComplete)
    {
        this.Id = Id;
        this.Name = Name;
        this.IsComplete = IsComplete;
    }
}




